# -*- coding: utf-8 -*-
import re
import json
import scrapy
import datetime

from test_project.items import Product

from scrapy.selector import Selector


class DnsShopSpider(scrapy.Spider):

    name = "dns_shop_spider"

    custom_settings = {
        'DOWNLOAD_DELAY': 0.35
    }

    main_url = "https://www.dns-shop.ru/catalog/17a892f816404e77/noutbuki/?p=1&i=1"

    api_url = "https://www.dns-shop.ru/catalog/category/catalog-ajax-load/?categoryGuid=17a892f8-1640-11e5-a679-00259074e77d&p=1&i=1"

    headers = {
        'Host': 'www.dns-shop.ru',
        'User-Agent': "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0'",
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'en-GB,en;q=0.5',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'Cache-Control': 'max-age=0',
    }

    cities = {
        'spb': '566ca284-5bea-11e2-aee1-00155d030b1f',
        'moscow': '30b7c1f3-03fb-11dc-95ee-00151716f9f5'
    }

    cookies = {
        'city_path': '',
        'current_path': '',
    }

    city_change = 'https://www.dns-shop.ru/ajax/change-city/?city_guid={}'

    def start_requests(self):
        for key, value in self.cities.items():
            yield scrapy.Request(self.city_change.format(value),
                                 self.get_cookies,
                                 meta={'city': key})

    def get_cookies(self, response):
        resp_cookies = response.headers.getlist('Set-Cookie')
        path = ','.join(resp_cookies).split('current_path=')[1].split(';')[0]
        cookies = {
            'city_path': response.meta['city'],
            'current_path': path,
        }
        yield scrapy.Request(self.api_url,
                             self.parse,
                             dont_filter=True,
                             headers=self.headers,
                             cookies=cookies,
                             meta={'cookies': cookies})

    def parse(self, response):
        cookies = response.headers.getlist('Set-Cookie')
        cookies = ','.join(cookies)
        json_response = json.loads(response.text)['html']
        content = Selector(text=json_response).xpath('//div[@class="title"]/a/@href').extract()
        for url in content:
            yield scrapy.Request(response.urljoin(url),
                                 self.parse_product,
                                 headers=self.headers,
                                 cookies=response.meta['cookies'],
                                 meta={'cookiejar': cookies},
                                 dont_filter=True,)
        if content:
            splited_url = response.url.split('&p=')
            next_page = splited_url[1].split('&')
            link = splited_url[0] + '&p=' + str(int(next_page[0]) + 1) + '&' + next_page[1]
            yield scrapy.Request(link, self.parse,
                                 headers=self.headers,
                                 cookies=response.meta['cookies'],
                                 dont_filter=True,
                                 meta=response.meta)

    def parse_product(self, response):
        item = Product()
        item["name"] = response.xpath('//h1/text()').extract_first()
        item["price"] = response.xpath('//span[@class="current-price-value"]/@data-price-value').extract_first()
        item['original_price'] = response.xpath('//s[@class="prev-price-total"]/@data-price-value').extract_first()
        item["currency"] = response.xpath('//meta[@itemprop="priceCurrency"]/@content').extract_first()
        item["category"] = response.xpath('//ol[@class="breadcrumb"]/li/a/span/text()').extract()[-1]
        stock = response.css('span.avail-text a span::text').extract_first()
        item["stock"] = True if re.findall(u'в \d+ магаз', stock) else False
        item["scan_time"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        table = response.xpath('//table[@class="table-params table-no-bordered"]/tr')
        for tr in table:
            key = tr.xpath('td[1]/div/span/text()').extract()
            value = tr.xpath('td[2]/text()').extract()
            if u'Цвет верхней крышки' in key:
                item["color"] = value[0]
            elif u'Код производителя' in key:
                item["sku"] = value[0]
        item["descriprion"] = response.xpath('//div[@itemprop="description"]/p/text()').extract_first()
        yield item
